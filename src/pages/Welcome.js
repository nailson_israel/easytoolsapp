import React, { 
	useState, 
	useEffect 
} from 'react';

import { 
	SafeAreaView, 
	StyleSheet, 
	Image, 
	AsyncStorage,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
   
import logo from '../../assets/images/easytools_horizontal.png';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default function Welcome({ navigation }) {

  async function handleSubmit() {
    navigation.navigate('RegisterName');
  }

	return (
		<>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
				<View style={styles.section}>
					<View style={styles.containerHorizontal}>
						<Image style={styles.logo} source={logo}/>
					</View>
					<View style={styles.container}>
						<View style={styles.box}>
							<Text style={styles.welcome}>Bem-vindo(a)!</Text>
							<Text 
								multiline={true}
								style={styles.biliontools}>
									Milhões de {'\n'}ferramentas todos {'\n'}os dias para você.
							</Text>       
							<TouchableOpacity onPress={handleSubmit} style={styles.subscribe}>
								<Text style={styles.inputSubscribe}>Cadastre-se</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.facebook}>
								<Text style={styles.inputBlack}>Entrar com o facebook</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.signin}>
								<Text style={styles.inputBlack}>Entrar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
      </SafeAreaView>
    </>
	)
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },

	section: {
		backgroundColor: '#ffd20d',
	},

  container: {
    flexGrow: 1,
    height:'100%',
		alignItems: 'center'
  },

  box: {
    marginTop: hp('10%'),
  },

  welcome: {
    fontSize: RFValue(24, 580),
    marginBottom:hp('3%'),
		marginTop:hp('3%'),
		fontFamily: 'Poppins-Regular'
  },

  biliontools: {
		fontSize: RFValue(34, 580),
    marginBottom:hp('15%'),
    fontFamily: 'Poppins-Medium'
  },

  inputSubscribe: { 
    color: '#fff',
    fontSize: RFValue(15, 580),
    textTransform: 'uppercase',
    fontFamily: 'Poppins-Medium'
  },
 
  inputBlack: { 
    color: '#000',
    fontSize: RFValue(15, 580),
    textTransform: 'uppercase',
    fontFamily: 'Poppins-Medium'    
  },  

  subscribe: {
    backgroundColor:'#2c98e0',
    borderRadius: 2,
    marginTop: hp('1%'),
    marginBottom: 20,
    borderRadius: 2,
    height: hp('6%'),
    fontSize: RFValue(16, 580),
    justifyContent: 'center',
    alignItems: 'center',
  },

  facebook: {
    borderWidth: 3,
    borderColor: '#000',
    paddingHorizontal: 5,
    height: hp('6%'),
    marginBottom: 20,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  signin: {
    paddingHorizontal: 5,
    height: hp('6%'),
    marginBottom: 20,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
	},
	
	containerHorizontal: {
		flexGrow: 1,
		flexDirection: 'row',
		paddingTop:hp('2%'),
	},

	logo: {
    width:150,

		marginLeft: hp('4%'),
	}
});

