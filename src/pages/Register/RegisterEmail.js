import React, { 
	useState, 
	useEffect 
} from 'react';

import { 
	SafeAreaView, 
	StyleSheet, 
	Image, 
	AsyncStorage,
  ScrollView,
  View,
	Text,
	TextInput,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Welcome({ navigation }) {

  async function handleBack() {
    navigation.navigate('RegisterEmail');
  }

	return (
		<>
			<SafeAreaView style={styles.container}>
					<View style={styles.boxback}>
				</View>
				<View style={styles.form}>
					<Text style={styles.title}>
						E o seu email?
					</Text>

					<Text style={styles.label}>Email</Text>
					<TextInput
						style={styles.input}
						autoCorrect={false}
						autoCapitalize="words"
					/>
					<View style={styles.line}/>
                    <Text style={styles.textmin}>
                        Gostaria de receber informações de ferramentas e promoções, incluindo 
                        descontos, pesquisa e inpirações por email, SMS, e telefone.
                    </Text>
				</View>
			</SafeAreaView>
		</>
  )   
}


const styles = StyleSheet.create({

  container: {
    flexGrow: 1,
    height: '100%',
		alignItems: 'center',
		backgroundColor: '#fff'
	},

	boxback: {
		alignSelf: 'stretch',
		paddingHorizontal: hp('3%'),
		paddingBottom: hp('3%'),
	},

	line: {
		borderBottomColor: '#9e9e9e',
		borderBottomWidth: 2,
		marginTop: 8,
		marginBottom: hp('5%'),
	},

	title: {
		fontFamily: 'Poppins-Medium',
		fontSize: RFValue(23, 580),
		paddingBottom: hp('5%'),
	},

	form: {
		alignSelf: 'stretch',
		paddingHorizontal: hp('3%'),
		marginTop: hp('4%')
	},

	label: {
		fontFamily: 'Poppins-Medium',
		fontSize: RFValue(13, 580),
		color: '#7a7a7a',
		marginBottom: hp('0%'),
	},

	input: {
		fontSize: RFValue(19, 580),
		fontFamily: 'OpenSans-Regular',
		marginBottom: hp('-2%'),
		textDecorationLine: 'none',
	},

	viewBottom: {
		flex: 1, 
		flexDirection: 'row-reverse',
		marginTop: hp('38%'),
	},

	go: {
		backgroundColor:'#ffd20d',
		borderRadius: 100,
		width: 48,
		height: 48,
		padding: 10,
		paddingLeft: 15,
		fontSize: 30,
		position: 'absolute',
		bottom: 0,
    },
    
    textmin: {
			fontSize: RFValue(10, 580),
			fontFamily: 'OpenSans-Regular',    
    }
})