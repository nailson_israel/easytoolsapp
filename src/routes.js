import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Welcome from './pages/Welcome';
import RegisterName from './pages/Register/RegisterName';
import RegisterEmail from './pages/Register/RegisterEmail';

const navigationOptions = {
	headerTintColor: '#ffd20d',
	headerStyle: {
		backgroundColor: '#fff',
		elevation: 0,       //remove shadow on Android
		shadowOpacity: 0,   //remove shadow on iOS
	}
}

const Routes = createAppContainer(
	createStackNavigator({
		Welcome: {
			screen: Welcome,
			navigationOptions: {
        header: null,
    	}
		},

		RegisterName: {
			screen: RegisterName,
			navigationOptions
		},

		RegisterEmail: {
			screen: RegisterEmail,
			navigationOptions
		}
	})
);

export default Routes;